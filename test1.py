#13. Заданное натуральное число от 1 до 1999 вывести римскими цифрами.
def to_roman(num):
    arab = [
            1000, 900, 500, 400,
            100, 90, 50, 40,
            10, 9, 5, 4,
            1
            ]
    rom = [
            "M", "CM", "D", "CD",
            "C", "XC", "L", "XL",
            "X", "IX", "V", "IV",
            "I"
            ]
    roman_num = " "
    for i in range(len(arab)):
        count = int(num / arab[i])
        roman_num += rom[i] * count
        num -= arab[i] * count
    return roman_num


n = int(input())
print(to_roman(n))

