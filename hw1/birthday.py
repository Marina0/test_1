day = int(input())
month = int(input())
year = int(input())
current_year = 2020
current_month = 5
current_day = 20
if current_month > month:
    print(current_year - year)
elif current_month < month:
    print(current_year - year - 1)
elif current_month == month:
    if current_day >= day:
        print(current_year - year)
    elif current_day < day:
        print(current_year - year - 1) 