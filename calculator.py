def plus(a, b):
    return a + b


def minus(a, b):
    return a - b


def multiply_by(a, b):
    return a * b


def divided_by(a, b):
    return a / b


def power(a, b):
    return a ** b


a = float(input("Enter the first number: "))
action = input("Select action: +, -, *, :, **: ")
b = float(input("Enter the second number: "))

if action == "+":
    print("is equal to ", plus(a, b))
elif action == "-":
    print("is equal to ", minus(a, b))
elif action == "*":
    print("is equal to ", multiply_by(a, b))
elif action == ":":
    print("is equal to ", divided_by(a, b))
elif action == "**":
    print("is equal to ", power(a, b))
else:
    print("Error")