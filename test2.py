import datetime

import sqlalchemy as sa
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql import func


Base = declarative_base()


class CommonMixture:
    id = sa.Column(sa.Integer, primary_key=True, autoincrement=True)
    created_at = sa.Column(
        sa.DateTime, nullable=False, default=datetime.datetime.now
    )


class Product(CommonMixture, Base):
    __tablename__ = 'products'

    name = sa.Column(sa.String(64), unique=True, nullable=False)
    price = sa.Column(sa.Float, nullable=False)


class WriteOff(CommonMixture, Base):
    __tablename__ = 'write_offs'

    product_name = sa.Column(
        sa.String, sa.ForeignKey(f'{Product.__tablename__}.name'),
    )
    amount = sa.Column(sa.Float, nullable=False)
    receptionist = sa.Column(sa.String(64), nullable=False)
    date = sa.Column(sa.Date, nullable=False)


class Role:
    def __init__(
        self, engine, name, host=None, port=None,
        username=None, password=None, **params
    ):
        dsn = f"{engine}://"
        if username is not None:
            dsn += username
        if password is not None:
            dsn += f":{password}"
        if username is not None:
            dsn += "@"
        if host is not None:
            dsn += f"{host}"
        if port is not None:
            dsn += f":{port}"
        dsn += f"/{name}"
        if params:
            dsn += "?"
        dsn += "&".join(f"{key}={value}" for key, value in params.items())

        self._engine = sa.create_engine(dsn)
        Base.metadata.create_all(self._engine)
        self._session = sessionmaker(bind=self._engine)()


class SupplyManager(Role):
    def add_product(self, name):
        return self._session.query(Product).filter(Product.name == name).first()


class InventorySpec(SupplyManager):   
    def change_price(self, name, price):
        product = self._session.query(Product).filter(
            Product.name == name
        ).first()
        product.name = name
        product.price = price
        

admin = InventorySpec(engine="sqlite", name="invent.db")
admin.add_product(name='T')
admin.change_price(name='K', price=100)









